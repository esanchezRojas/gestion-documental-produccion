//Recibe un estado y devuelve una matriz con los datos de cada fila
function request(estado,code) {
  //estado = "devolucion"
  
  var cumplimi = "";
  var devol = "";
  var confor = "";
  if(estado =="No cumple"){
    estado = "Entregado";
    cumplimi = "No cumple";
  }else if(estado == "Devolucion"){
    estado = "Entregado";
    devol = "Sí";
    confor = "Conforme";
  }
  
    var spreadsheet = SpreadsheetApp.openById(ssId);
    var sheeta = spreadsheet.getSheetByName(ssDb);
    var LastR = sheeta.getLastRow();
    var datos = sheeta.getDataRange().getValues();
    var longitud = sheeta.getRange("A2:A" + LastR).getValues();
    var matriz = [];
    var sesionActiva = Session.getActiveUser().getEmail();
    //var sesionActiva = "scubides@sedic.com.co"
    
    //var nomComparacion = "Edwin Alejandro Sanchez Rojas";
    //var estado = "Pendiente";
    var correosGD =[];
    var listaCorreos = listas("GD");
      
    for(i=0;i<listaCorreos.length;i++){
      correosGD.push(listaCorreos[i][0])
    }

    for (var i = 0; i <= longitud.length; i++) {


        let codigo = datos[i][0];
        let fechaSoli = datos[i][2];
        let solicitante = datos[i][4];
        let cargo = datos[i][5];
        let numContacto = datos[i][6];
        let fechaEntre = datos[i][14];
        let estadoAprobacion = datos[i][11];
        let estadoEntrega = datos[i][12];
        let aprobador = datos[i][9];
        let fechaReque = datos[i][14];
        let dias = datos[i][26];
        let cumpEntrega = datos[i][13];
        let requiDev = datos[i][7]
        
        let conformidad = datos[i][15];
        let obsGD = datos[i][17];
        let conforDevol = datos[i][20];
        //var listaCorreos =  ""
       
        
        if (correosGD.indexOf(sesionActiva) > -1) {
          

            if (estadoEntrega == estado && codigo != code ) {


                if (fechaSoli == "") {

                    var fechaSolicitud = "";
                } else {

                    var fechaSolicitud = Utilities.formatDate(fechaSoli, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'dd-MM-yyyy HH:mm:ss');
                  

                }

                if (fechaEntre == "") {

                    var fechaEntrega = "";

                } else {

                    var fechaEntrega = Utilities.formatDate(fechaEntre, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'dd-MM-yyyy HH:mm:ss');

                }
              
                if (fechaReque == "") {

                    var fechaRequer = "";

                } else {

                    var fechaRequer = Utilities.formatDate(fechaReque, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'dd-MM-yyyy');

                }
              if(cumplimi == "No cumple"){
                if(estadoEntrega == "Entregado" && cumpEntrega =="No cumple"){ 
                   let fila = [codigo, fechaSolicitud, solicitante, cargo, numContacto, estadoAprobacion, aprobador, dias];
                   matriz.push(fila);
                }
              }
              else if(requiDev == devol && cumpEntrega =="Cumple" && confor != conforDevol ){
                 let fila = [codigo, fechaSolicitud, solicitante, cargo, numContacto, aprobador , fechaRequer, obsGD];
                matriz.push(fila);
              }
              else if(estadoEntrega == "Entregado" && cumpEntrega =="Cumple" && devol ==""){ 
                let fila = [codigo, fechaSolicitud, solicitante, cargo, numContacto, estadoAprobacion, aprobador, estadoEntrega, fechaRequer, obsGD, conformidad];
                matriz.push(fila);
              }else if(estadoEntrega == "Pendiente"){
                let fila = [codigo, fechaSolicitud, solicitante, cargo, numContacto, estadoAprobacion, aprobador, dias];
                matriz.push(fila);
              }

            }
        }
    }
    Logger.log(matriz)
    return matriz;

}

//Busca el código que trae desde el front y lo compara con el código de la base de datos detalle
function getBuscar2(codigoFront, cump) {
    
  if(cump == "devolucion"){
    
    cump = "Cumple"
    var requ = "Sí"
    var tipo = "devolucion";
    
   }
  
    
    var spreadsheet = SpreadsheetApp.openById(ssId);
    var sheeta = spreadsheet.getSheetByName(ssDbD);
    var LastR = sheeta.getLastRow();
    var datos = sheeta.getDataRange().getValues();
    var longitud = sheeta.getRange("A2:A" + LastR).getValues();
    var matriz = [];
    var sesionActiva = Session.getActiveUser().getEmail();
    var correosGD =[];
    var listaCorreos = listas("GD");
      
    for(i=0;i<listaCorreos.length;i++){
      correosGD.push(listaCorreos[i][0])
    }


    for (var i = 0; i <= longitud.length; i++) {

        let codigo = datos[i][0];
        let item = datos[i][1];
        let tipoArchivo = datos[i][7];
        let finalidad = datos[i][13];
        let requiere = datos[i][14];
        let requiereDev = datos[i][16];
        let descrip = datos[i][12];
        let estadoAprov = datos[i][20];
        let aprobador = datos[i][18];
        let segundoAprov = datos[i][19];
        let conformidad = datos[i][24];
        let razon = datos[i][25];
        let razonDevol = datos[i][28];
        let estadoDevol = datos[i][26];
        let conforDevol = datos[i][27];
        

        let correoUser = datos[i][3];
      
      if(tipo != undefined){
        if (correosGD.indexOf(sesionActiva) > -1 && codigo == codigoFront && requiereDev == requ) {
        
            let fila = [codigo, item,razonDevol, tipoArchivo,descrip,finalidad, requiere, requiereDev,estadoDevol, conforDevol];
                                            
            matriz.push(fila);
        
        }
        
      }else if(cump != undefined){
        if (correosGD.indexOf(sesionActiva) > -1 && codigo == codigoFront && conformidad == cump) {



            let fila = [codigo, item, tipoArchivo, finalidad, requiere, requiereDev, descrip, estadoAprov, razon];
            matriz.push(fila);
        }
      }else{
        if (correosGD.indexOf(sesionActiva) > -1 && codigo == codigoFront) {



            let fila = [codigo, item,tipoArchivo, finalidad, requiere, requiereDev, descrip, estadoAprov, segundoAprov];
            matriz.push(fila);
        }
      
      }
    }
      
    return matriz;

}

function separarCadena(correos) {

    //var correos = "ddd,jjj,kkk,qqq";
    var lstaCorreos = correos.split(', ');
    return lstaCorreos;

}

function gdPostServer(estado, comentario, codigo, cumplimiento){
  
    //funciones y metodos de google
    var ss = SpreadsheetApp.openById(ssId);
    var db = ss.getSheetByName(ssDb);
    var datos = db.getDataRange().getValues();
  
    //configuracion de la hora actual
    var hoy = new Date();
    var addedTime = Utilities.formatDate(hoy, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'dd/MM/yyyy');
    
    
    for(i=0;i<datos.length;i++){
      if(datos[i][0].toString() == codigo){
        
         var fila = i + 1
         Logger.log(fila)
         db.getRange(fila, 13).setValue(estado)
         db.getRange(fila, 18).setValue(comentario)
         db.getRange(fila, 15).setValue(addedTime)
         if(cumplimiento != undefined){
           db.getRange(fila, 14).setValue(cumplimiento)
           db.getRange(fila, 16).setValue("Conforme")
         }
         break
      }
    } 
}

function info(codigoFront, fechaini, fechafin, estadoFront) {

    //var codigoFront = "202011182";
    // var fechaini = "";
    // var fechafin = "";


    var spreadsheet = SpreadsheetApp.openById(ssId);
    var sheeta = spreadsheet.getSheetByName(ssDb);
    var LastR = sheeta.getLastRow();
    var datos = sheeta.getDataRange().getValues();
    var longitud = sheeta.getRange("A2:A" + LastR).getValues();
    var matriz = [];
    var sesionActiva = Session.getActiveUser().getEmail();
    var correosGD =[];
    var listaCorreos = listas("GD");
      
    for(i=0;i<listaCorreos.length;i++){
      correosGD.push(listaCorreos[i][0])
    }


    if (fechaini > fechafin && fechafin != "") {

        // Logger.log("error fecha");

    } else {
        for (var i = 0; i <= longitud.length; i++) {

            let codigo = datos[i][0];
            let fechaSoli = new Date(datos[i][2]);
            let solicitante = datos[i][4];
            let cargo = datos[i][5];
            let numContacto = datos[i][6];
            let fechaEntre = datos[i][14];
            let estadoAprobacion = datos[i][11];
            let aprobador = datos[i][9];
            let estadoEntrega = datos[i][12];
            let fechaReque = datos[i][14];
            let dias = datos[i][26];
          
            let conformidad = datos[i][15];
            let obsGD = datos[i][17];
            
            if (fechaReque == "") {

              var fechaRequer = "";

            } else {

              //var fechaRequer = Utilities.formatDate(fechaReque, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'dd-MM-yyyy');
              Logger.log(fechaReque)
            }

            // Logger.log(correoUser);
            // Logger.log("sesion activa "+sesionActiva);




            var fechaS = Utilities.formatDate(fechaSoli, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'yyyy-MM-dd');


            if (correosGD.indexOf(sesionActiva) > -1) {


                if (codigo == codigoFront) {



                    if (fechaSoli == "") {

                        var fechaSolicitud = "";
                    } else {

                        var fechaSolicitud = Utilities.formatDate(fechaSoli, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'dd-MM-yyyy HH:mm:ss');

                    }

                    if (fechaEntre == "") {

                        var fechaEntrega = "";

                    } else {

                        var fechaEntrega = Utilities.formatDate(fechaEntre, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'dd-MM-yyyy HH:mm:ss');

                    }


                  if(estadoFront == "Pendiente"){
                    let fila = [codigo, fechaSolicitud, solicitante, cargo, numContacto, estadoAprobacion, aprobador, dias];
                    matriz.push(fila);
                  }else{
                    let fila = [codigo, fechaSolicitud, solicitante, cargo, numContacto, estadoAprobacion, aprobador, estadoEntrega, fechaRequer, obsGD, conformidad];
                    matriz.push(fila);
                  
                  }




                } else if (codigoFront == "") {



                    if (fechaini <= fechaS && fechaS <= fechafin) {

                        let tiempo = Utilities.formatDate(fechaSoli, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'yyyy-MM-dd HH:mm:ss');

                        let fila = [codigo, tiempo, aprobador, estadoSoliciud, estadoEntrega, fechaEntrega, estadoDevol, fechaDevol];
                        matriz.push(fila);
                        Logger.log("opcion 1 ");


                    } else if (fechaini == "" && fechaS <= fechafin) {

                        let tiempo = Utilities.formatDate(fechaSoli, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'yyyy-MM-dd HH:mm:ss');

                        let fila = [codigo, tiempo, aprobador, estadoSoliciud, estadoEntrega, fechaEntrega, estadoDevol, fechaDevol];
                        matriz.push(fila);
                        Logger.log("opcion 2 ");


                    } else if (fechafin == "" && fechaS >= fechaini) {

                        let tiempo = Utilities.formatDate(fechaSoli, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'yyyy-MM-dd HH:mm:ss');

                        let fila = [codigo, tiempo, aprobador, estadoSoliciud, estadoEntrega, fechaEntrega, estadoDevol, fechaDevol];
                        matriz.push(fila);
                        Logger.log("opcion 3 ");

                    } else {

                        /*
                          let tiempo = Utilities.formatDate(fechaSoli, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'yyyy-MM-dd HH:mm:ss');

                          let fila = [codigo, tiempo, aprobador, estadoSoliciud, estadoEntrega, fechaEntrega, estadoDevol, fechaDevol];
                          matriz.push(fila);
                         Logger.log("opcion 4 ");*/

                    }

                }
                // Logger.log("codigo error");

            }

        }

    }
    // Logger.log("opcion fuera " + matriz);
    return matriz;
}

function indicadoresBD(){
  var ss = SpreadsheetApp.openById(ssId);
  var config = ss.getSheetByName(ssConfig);
  var NoCon = config.getRange("T2").getValue();
  var Pendi = config.getRange("U2").getValue();
  var devol = config.getRange("V2").getValue();
  var indicadores = [NoCon, Pendi, devol]
  Logger.log(indicadores)
  return indicadores
}


//Envia estado en la columna de estado de aprobación
function enviarEstado(codigo, item, est) {

    var cod = codigo;
    var it = item;
    var estado = est;
    //Logger.log(cod,it,estad)

    editarPosicionEstado(cod, it, estado, 28);

}

//Envia comentario si la opción de aprobacion es rechazado
function enviarComentario(codigo, item, comen) {

   var cod = codigo;
   var it = item;
   var coment = comen;
    //Logger.log(cod,it,estad)
     //cod = 202011182;
     //it = 3;
     //coment = "Esto es un comentario";


    editarPosicion(cod, it, coment, 29);

}




function editarPosicionEstado(code, item, dato, colum) {


    //var code = "202010295";
    //var estado = "Aprovaddddrrrrrrrrrrrr"
    //var colum = 21; 
    //var item = "Hoja de Vida";

    var spr = SpreadsheetApp.openById(ssId);
    var hoja = spr.getSheetByName(ssDbD);

    //Rango: (primera fila, numero columna,todas las filas)
    //columna de codigo
    var datosCodigo = hoja.getRange(1, 1, hoja.getLastRow()).getValues();
    var datosItem = hoja.getRange(1, 2, hoja.getLastRow()).getValues();
    var datosEstado = hoja.getRange(1, 21, hoja.getLastRow()).getValues();



    var sw = false;

    for (var i = 0; i < datosCodigo.length; i++) {
        for (var j = 0; j < datosCodigo.length; j++) {
            if (code == datosCodigo[i][j] && item == datosItem[i][j] && dato != datosEstado[i][j]) {
                var fila = i + 1;

                sw = true;
                break;
            }
        }
    }

    if (sw == true) {
      
      hoja.getRange(fila, colum).setValue(dato);
      
      /*
      
      if (dato == "No conforme"){
      
       let columEstado = 24;
       let estadoConfor = "Pendiente" ;
       hoja.getRange(fila, columEstado).setValue(estadoConfor);
        
      }else if(dato == "Conforme"){
       let columEstado = 24;
       let estadoConfor = "Entregado" ;
       hoja.getRange(fila, columEstado).setValue(estadoConfor);
      */
      }

        

       
}



function editarPosicion(code, item, dato, colum) {


    // var code = "202011182";
    // var dato = "Aprovaddddrrrrrrrrrrrr"
    // var colum = 28; 
    // var item = "1";

    var spr = SpreadsheetApp.openById(ssId);
    var hoja = spr.getSheetByName(ssDbD);

    //Rango: (primera fila, numero columna,todas las filas)
    //columna de codigo
    var datosCodigo = hoja.getRange(1, 1, hoja.getLastRow()).getValues();
    var datosItem = hoja.getRange(1, 2, hoja.getLastRow()).getValues();
    var datosEstado = hoja.getRange(1, 2, hoja.getLastRow()).getValues();



    var sw = false;

    for (var i = 0; i < datosCodigo.length; i++) {
        for (var j = 0; j < datosCodigo.length; j++) {
            if (code == datosCodigo[i][j] && item == datosItem[i][j]) {
                var fila = i + 1;

                sw = true;
                break;
            }
        }
    }

    if (sw == true) {
      
       hoja.getRange(fila, colum).setValue(dato);
      
        if (colum == 20 || colum == 21) {

            var columnaEstado = 22;
            var hoy = new Date();
            var fecha = Utilities.formatDate(hoy, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'dd/MM/yyyy HH:mm:ss');
            hoja.getRange(fila, columnaEstado).setValue(fecha);
        }

       // Logger.log(fila);

    } else {

        return "";
    }
}




//Envia el estado global a la Bd principal
function estadoGlobalConfor(codigo, estados, items) {

    //var codigo = 202011182;
    //var estados = ["Conforme","Conforme"];
    //var items = ["1","2"];


    var spr = SpreadsheetApp.openById(ssId);
    var hoja = spr.getSheetByName(ssDb);

    //Hoja 2 (BD-Detallada)
    var spreadsheet = SpreadsheetApp.openById(ssId);
    var sheeta = spreadsheet.getSheetByName(ssDbD);
    var LastR = sheeta.getLastRow();
    var datos = sheeta.getDataRange().getValues();
    var longitud = sheeta.getRange("A2:A" + LastR).getValues();
    var estadosBack = [];


    for (i = 0; i < longitud.length; i++) {

        let codigoBack = datos[i][0];
        let codidigoFront = codigo;
        //let itemFront = items[i]; 
        let estadoConfor = datos[i][26];
        Logger.log(requiere)

        // Logger.log("Back "+codigoBack +"Front "+codidigoFront);

        if (codigoBack == codidigoFront) {

            estadosBack.push(estadoConfor);

        }
    }
    Logger.log("estados Back: " + estadosBack);

    for (k = 0; k < longitud.length; k++) {
        var requiere = datos[k][16];

        for (j = 0; j < items.length; j++) {

            if (k == (items[j] - 1)) {

                estadosBack.splice(k, 1, estados[j]);
            }

        }
    }
    
  
    Logger.log(estadosBack + " despues de splice");

    if (estadosBack.indexOf("No conforme") > -1){
        var conformidad = "No conforme";
        var estadoConfo = "Pendiente";
        var estadoGlobal = "Abierto";
    } else if(estadosBack.indexOf("") > -1){
        var conformidad = "";
        var estadoConfo = "Pendiente";
        var estadoGlobal = "Abierto";
      
    }else{
  
        var conformidad = "Conforme";
        var estadoConfo = "Devuelto";
        var estadoGlobal = "Finalizado";
        var fechaConfor = new Date();
        var addedTime = Utilities.formatDate(fechaConfor, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'dd/MM/yyyy');
        editarPosicionDb(codigo, addedTime, 20);
    }
  
    editarPosicionDb(codigo, conformidad, 21);
    editarPosicionDb(codigo, estadoConfo, 19);
    editarPosicionDb(codigo, estadoGlobal, 9);

}

//Envia valor a la Bd principal
function editarPosicionDb(code, dato, colum) {


    //var code = "202010291";
    //var dato = "esto es un correo"
    //var colum = 10; 
    //var item = "Hoja de Vida";

    var spr = SpreadsheetApp.openById(ssId);
    var hoja = spr.getSheetByName(ssDb);

    //Rango: (primera fila, numero columna,todas las filas)
    //columna de codigo
    var datosCodigo = hoja.getRange(1, 1, hoja.getLastRow()).getValues();



    var sw = false;

    for (var i = 0; i < datosCodigo.length; i++) {
        for (var j = 0; j < datosCodigo.length; j++) {
            if (code == datosCodigo[i][j]) {
                var fila = i + 1;

                sw = true;
                break;
            }
        }
    }

    if (sw == true) {

        hoja.getRange(fila, colum).setValue(dato);



        //Logger.log(fila);

    } else {

        return "";
    }
}