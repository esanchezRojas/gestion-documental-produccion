
  var rutaWeb = ScriptApp.getService().getUrl();
  var ssId = "1_48ODkoAZdzczN6cOaMCsiErwSeoE7q_C-YX2ZrsFl4";
  var ssDb = "BD";
  var ssDbD = "BD-Detallada";
  let ssConfig = "Config";


function userId() {
    //obtenemos correo y nombre
    var correo = Session.getActiveUser().getEmail();
    var nombre = ContactsApp.getContact(correo).getFullName();
    return ([nombre, correo])
}

function doGet(e) {

    var correo = Session.getActiveUser().getEmail();
    var correosGD = [];
    var page = e.parameter.p || 'Devoluciones';
    //var page = e.parameter.p || 'Pendientes';
  

    //correos permitidos para ingresar a la página
    correosGD = listas("GD");

    var sw = false;

    //Logger.log(correosGD[0][0]);
    for (var i = 0; i < correosGD.length; i++) {

        if (correo == correosGD[i][0]) {

            sw = true;

            return HtmlService.createTemplateFromFile(page).evaluate()
                .setSandboxMode(HtmlService.SandboxMode.IFRAME)
                .addMetaTag('viewport', 'width=device-width, initial-scale=1, maximum-scale=2.0, user-scalable=yes')
                .setTitle('SEDIC')
                .setFaviconUrl('https://drive.google.com/uc?id=11VmVU-VhcrAi-_GjaxM048OZoL-2WpeM#.ico');

            break;
        }

    }

    if (sw == false) {

        return HtmlService.createTemplateFromFile('ErrorPage').evaluate()
            .setSandboxMode(HtmlService.SandboxMode.IFRAME)
            .addMetaTag('viewport', 'width=device-width, initial-scale=1, maximum-scale=2.0, user-scalable=yes')
            .setTitle('SEDIC')
            .setFaviconUrl('https://drive.google.com/uc?id=11VmVU-VhcrAi-_GjaxM048OZoL-2WpeM#.ico');
    }

}


function include(filename) {
    return HtmlService.createHtmlOutputFromFile(filename).getContent();

}

function prueba() {
    var lista = listas("admin");
    Logger.log(listas("GD"));

}


function listas(lista) {
    var ss = SpreadsheetApp.openById(ssId);
    var config = ss.getSheetByName(ssConfig);

    //Numero de elementos por cada lista el +2 es para traer los valores directamente
    var nTipos = config.getRange("A2").getValue() + 2;
    var nProcesos = config.getRange("B2").getValue() + 2;
    var nProyectos = config.getRange("C2").getValue() + 2;
    var nDocumentos = config.getRange("D2").getValue() + 2;
    var nRequiere = config.getRange("E2").getValue() + 2;
    var nEstados = config.getRange("H2").getValue() + 2;
    var nAdmin = config.getRange("L2").getValue() + 2;
    var nGD = config.getRange("O2").getValue() + 2;

    //listas
    var tipos = config.getRange("A3:A" + nTipos).getValues();
    var procesos = config.getRange("B3:B" + nProcesos).getValues();
    var proyectos = config.getRange("C3:C" + nProyectos).getValues();
    var documentos = config.getRange("D3:D" + nDocumentos).getValues();
    var requiere = config.getRange("E3:E" + nRequiere).getValues();
    var estados = config.getRange("H3:H" + nEstados).getValues();
    var admin = config.getRange("L3:L" + nAdmin).getValues();
    var GD = config.getRange("O3:O" + nGD).getValues();

    //retorno
    switch (lista) {
        case "tipos":
            return tipos
            break;
        case "proyectos":
            return proyectos
            break;
        case "procesos":
            return procesos
            break;
        case "documentos":
            return documentos
            break;
        case "requiere":
            return requiere
            break;
        case "estados":
            return estados
            break;
        case "admin":
            return admin
            break;
        case "GD":
            return GD
            break;
    }
}

function userDatos() {
    var ss = SpreadsheetApp.openById(ssId);
    var db = ss.getSheetByName(ssDb);
    var rowdb = db.getLastRow();

    //obtenemos correo y nombre
    var correo = Session.getActiveUser().getEmail();
    var nombre = ContactsApp.getContact(correo).getFullName();

    //tomamos la información de la hora actual
    var hoy = new Date();
    var addedTime = Utilities.formatDate(hoy, SpreadsheetApp.getActive().getSpreadsheetTimeZone(), 'dd/MM/yyyy HH:mm:ss');
    var dd = hoy.getDate().toString();
    var mm = (hoy.getMonth() + 1).toString();
    var yyyy = (hoy.getFullYear()).toString();

    //armamos el codigo
    var codigo = yyyy + mm + dd + rowdb;

    var arrayUser = [addedTime, codigo, correo, nombre]

    return arrayUser
}

